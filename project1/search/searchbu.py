# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())


    """
    "*** YOUR CODE HERE ***"

    parent_node = {}
    stack_list = []
    visited = []

    visited.append(problem.getStartState())
    for each in problem.getSuccessors(problem.getStartState()):
        stack_list.append(each)

    while stack_list:
        current = stack_list.pop()

        if problem.isGoalState(current[0]):
            print "goal"
            path = make_path(current, parent_node)
            return path

        if current[0] not in visited:
            visited.append(current[0])

            for each in problem.getSuccessors(current[0]):
                if each[0] not in visited:
                    stack_list.append(each)
                    parent_node[each] = current

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    parent_node = {}
    visited = []
    the_queue = util.Queue()
    fringe_nodes = []

    visited.append(problem.getStartState())
    for each in problem.getSuccessors(problem.getStartState()):
        the_queue.push(each)
        fringe_nodes.append(each[0])


    while not the_queue.isEmpty():
        current = the_queue.pop()

        if problem.isGoalState(current[0]):
            print "Goal!"
            final_path = make_path(current, parent_node)
            return final_path

        if current not in visited:
            visited.append(current[0])

            for each in problem.getSuccessors(current[0]):
                if each[0] not in visited and each[0] not in fringe_nodes:
                    the_queue.push(each)
                    parent_node[each] = current
                    fringe_nodes.append(each[0])

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"

    parent_node = {}
    ucs_list = []
    visited = []
    the_queue = util.PriorityQueue()
    fringe_nodes = []
    cost_to_goal = {}

    visited.append(problem.getStartState())
    for each in problem.getSuccessors(problem.getStartState()):
        the_queue.update(each, each[2])
        fringe_nodes.append(each[0])
        if problem.isGoalState(each[0]):
            cost_to_goal[each] = each
            fringe_nodes.remove(each[0])

    while not the_queue.isEmpty():
        current = the_queue.pop()
        for each in cost_to_goal.keys():
            if get_cost_to_node(current, parent_node, problem) >= \
                                                        cost_to_goal[each][2]:
                ucs_list.append(each[1])
                return ucs_list

        if problem.isGoalState(current[0]):
            print "Goal"
            final_path = make_path(current, parent_node)
            return final_path

        if current not in visited:
            visited.append(current[0])

            for each in problem.getSuccessors(current[0]):
                if each[0] not in visited and each[0] not in fringe_nodes:
                    fringe_nodes.append(each[0])
                    parent_node[each] = current
                    the_queue.update(each, \
                        (get_cost_to_node(each, parent_node, problem) + each[2]))

def get_num_neighs(current, problem):
    count = 0
    for each in problem.getSuccessors(current[0]):
        count += 1
    return count

def get_cost_to_node(current, parent, problem):
    sumcost = 0
    start = problem.getStartState()
    while current in parent.keys() and current is not start:
        sumcost += parent[current][2]
        current = parent[current]

    return sumcost

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"

    # util.raiseNotDefined()

    visited = []
    parent_node = {}
    path_queue = util.PriorityQueue()
    fringe_nodes = []

    visited.append(problem.getStartState())
    for each in problem.getSuccessors(problem.getStartState()):
        # path_queue.push(each, heuristic(each[0], problem))
        # print heuristic(each[0], problem)
        # print each
        path_queue.update(each, heuristic(each[0], problem))
        fringe_nodes.append(each[0])

    while not path_queue.isEmpty():
        current = path_queue.pop()
        # path_queue.update(current, )

        if problem.isGoalState(current[0]):
            print "Goal!"
            path = make_path_a(parent_node, current, problem, heuristic)
            # print 'path', path
            return path

        if current not in visited:
            visited.append(current)

            for each in problem.getSuccessors(current[0]):
                if each[0] not in visited and each[0] not in fringe_nodes:
                    # path_queue.push(each, heuristic(each[0], problem))
                    path_queue.update(each, heuristic(each[0], problem))
                    fringe_nodes.append(each[0])
                    parent_node[each] = current

#   __*EW
#   T

def make_path_a(parent, current, problem, heuristic=nullHeuristic):
    final_path = []
    final_path.append(current[1])
    # print current[1]
    # i = 0
    while current in parent.keys():
        # A* causes infinite loop in here.... dafuq....
        # print "cant be infinite..."
        # i += 1
        # print i
        current = parent[current]
        final_path.append(current)
        print final_path, "\n\n"
        print heuristic(current[0], problem)

    final_path = list(reversed(final_path))
    return final_path


def make_path(current, parent):
    final_path = []
    final_path.append(current[1])
    while current in parent.keys():
        current = parent[current]
        final_path.append(current[1])

    final_path = list(reversed(final_path))
    return final_path


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
