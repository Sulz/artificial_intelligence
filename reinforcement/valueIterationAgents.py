# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import mdp, util

from learningAgents import ValueEstimationAgent

class ValueIterationAgent(ValueEstimationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A ValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100):
        """
          Your value iteration agent should take an mdp on
          construction, run the indicated number of iterations
          and then act according to the resulting policy.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state, action, nextState)
              mdp.isTerminal(state)
        """
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter() # A Counter is a dict with default 0
        self.count = 0
        # Write value iteration code here

        "*** YOUR CODE HERE ***"
        states = mdp.getStates()
        for i in range(self.iterations):
            self_vals = util.Counter()

            # Updating every state
            for state in states:
                max_sum = float('-inf')
                actions = self.mdp.getPossibleActions(state)

                # Finding best value of each sucessor state, given by taking action
                for action in actions:
                    nxt_val = self.computeQValueFromValues(state, action)

                    # If true, nxt_val is indicator of better successor state
                    if nxt_val > max_sum:
                        max_sum = nxt_val

                        # Update current state value with value based on optimal neigh
                        self_vals[state] = max_sum

            # Update states with new values
            self.values = self_vals

    def getValue(self, state):
        """
          Return the value of the state (computed in __init__).
        """
        return self.values[state]


    def computeQValueFromValues(self, state, action):
        """
          Compute the Q-value of action in state from the
          value function stored in self.values.
        """
        "*** YOUR CODE HERE ***"

        value = 0

        # Sum of U(s') for each s' in T(s, a, s') based on previous iteration val.
        #       Multiple due to stochastic world
        for nxt_state, prob in self.mdp.getTransitionStatesAndProbs(state, action):
                value += prob * (self.mdp.getReward(state, action, nxt_state) \
                                + (self.discount * self.getValue(nxt_state)))

        return value

    def computeActionFromValues(self, state):
        """
          The policy is the best action in the given state
          according to the values currently stored in self.values.

          You may break ties any way you see fit.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return None.
        """
        "*** YOUR CODE HERE ***"

        maxQ = -float('inf')
        best_action = None
        # Finding best action to take from current state.
        for action in self.mdp.getPossibleActions(state):
            q_val = self.computeQValueFromValues(state, action)

            if q_val > maxQ:
                maxQ = q_val
                # best action is associated with highest Q value.
                best_action = action

        return best_action

    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    def getAction(self, state):
        "Returns the policy at the state (no exploration)."
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)
