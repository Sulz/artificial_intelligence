# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        sum_score = 0
        count = 0
        ghost_score = 0
        food_score = 0
        for ghosts in newGhostStates:
            ghost_score += ghost_heur(ghosts.getPosition(), newPos)
            count += 1

        food_score += (2 * food_heur(newPos, newFood))

        sum_score = food_score + ghost_score
        return successorGameState.getScore() + sum_score

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """

        def rec_minmax(curr_state, depth, agent_num, num_agents):
            lots = float('inf')
            if depth == 0 or not check_terminator(curr_state, agent_num):
                return [self.evaluationFunction(curr_state), None]

            if agent_num == num_agents:
                nxt_agent = 0
            else:
                nxt_agent = agent_num + 1

            if not agent_num: # Pacmans turn
                best_value = -lots
                best_move = None
                for moves in curr_state.getLegalActions(agent_num):
                    suc_game_state = curr_state.generateSuccessor(agent_num, moves)
                    child_val = rec_minmax(suc_game_state, depth, \
                                                nxt_agent, num_agents)
                    if best_value < child_val[0]:
                        best_value = child_val[0]
                        best_move = moves

                        if child_val[1] == None:
                            child_val[1] = best_move

                return [best_value, best_move]

            elif agent_num > 0:
                best_value = lots
                best_move = None
                for moves in curr_state.getLegalActions(agent_num):
                    suc_game_state = curr_state.generateSuccessor(agent_num, moves)

                    # Decrement depth when the next player is pacman.
                    if not nxt_agent:
                        child_val = rec_minmax(suc_game_state, depth - 1, \
                                                nxt_agent, num_agents)
                    else:
                        child_val = rec_minmax(suc_game_state, depth, \
                                                nxt_agent, num_agents)
                    if best_value > child_val[0]:
                        best_value = child_val[0]
                        best_move = moves

                        if child_val[1] == None:
                            child_val[1] = best_move

                return [best_value, best_move]

        return rec_minmax(gameState, self.depth, 0, gameState.getNumAgents() - 1)[1]

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"

        def alpha_beta_minimax(curr_state, depth, agent_num, num_agents, alpha, beta):
            lots = float('inf')
            if depth == 0 or not check_terminator(curr_state, agent_num):
                return [self.evaluationFunction(curr_state), None]

            if agent_num == num_agents:
                nxt_agent = 0
            else:
                nxt_agent = agent_num + 1

            if not agent_num: # Pacmans turn
                best_value = -lots
                best_move = None
                for moves in curr_state.getLegalActions(agent_num):
                    suc_game_state = curr_state.generateSuccessor(agent_num, moves)
                    child_val = alpha_beta_minimax(suc_game_state, depth, \
                                            nxt_agent, num_agents, alpha, beta)
                    if best_value < child_val[0]:
                        best_value = child_val[0]
                        best_move = moves

                        if child_val[1] == None:
                            child_val[1] = best_move

                    alpha = max(alpha, best_value)

                    if beta < alpha:
                        break

                return [best_value, best_move]

            elif agent_num > 0:
                best_value = lots
                best_move = None
                for moves in curr_state.getLegalActions(agent_num):
                    suc_game_state = curr_state.generateSuccessor(agent_num, moves)

                    # Decrement depth when the next player is pacman.
                    if not nxt_agent:
                        child_val = alpha_beta_minimax(suc_game_state, depth - 1, \
                                                nxt_agent, num_agents, \
                                                alpha, beta)
                    else:
                        child_val = alpha_beta_minimax(suc_game_state, depth, \
                                                nxt_agent, num_agents, \
                                                alpha, beta)
                    if best_value > child_val[0]:
                        best_value = child_val[0]
                        best_move = moves

                        if child_val[1] == None:
                            child_val[1] = best_move

                    beta = min(beta, best_value)

                    if beta < alpha:
                        break

                return [best_value, best_move]

        return alpha_beta_minimax(gameState, self.depth, 0, \
                                    gameState.getNumAgents() - 1, \
                                    -float('inf'), float('inf'))[1]

        # util.raiseNotDefined()

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """

        def rec_minmax(curr_state, depth, agent_num, num_agents):
            lots = float('inf')
            if depth == 0 or not check_terminator(curr_state, agent_num):
                return [self.evaluationFunction(curr_state), None]

            if agent_num == num_agents:
                nxt_agent = 0
            else:
                nxt_agent = agent_num + 1

            if not agent_num: # Pacmans turn
                best_value = -lots
                best_move = None
                for moves in curr_state.getLegalActions(agent_num):
                    suc_game_state = curr_state.generateSuccessor(agent_num, moves)
                    child_val = rec_minmax(suc_game_state, depth, \
                                                nxt_agent, num_agents)

                    if best_value < child_val[0]:
                        best_value = child_val[0]
                        best_move = moves

                        if child_val[1] == None:
                            child_val[1] = best_move

                return [best_value, best_move]

            elif agent_num > 0:
                best_value = 0
                best_move = None
                count = 0
                for moves in curr_state.getLegalActions(agent_num):
                    suc_game_state = curr_state.generateSuccessor(agent_num, moves)

                    """Decrement depth when the next player is pacman."""
                    if not nxt_agent:
                        best_value = best_value + \
                                        rec_minmax(suc_game_state, depth - 1, \
                                                        nxt_agent, num_agents)[0]
                    else:
                        best_value = best_value + \
                                        rec_minmax(suc_game_state, depth, \
                                                        nxt_agent, num_agents)[0]

                    count += 1
                    best_move = moves

                best_value = float(best_value/count)
                return [best_value, best_move]

        return rec_minmax(gameState, self.depth, 0, gameState.getNumAgents() - 1)[1]

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: 
          Only taking into account the food and ghosts.
          -  Food: Evaluate state based on the recipricol of the sum of the manhattan distances to 
                    each piece of food.

          - Ghosts:
              - If the ghosts are > 5 manhattan distance away, ignore.
              - If ghosts are < 2 manhattan distance away, flee! (using infinity)
              - Otherwise, return negative reciprical of manhattan distance to ghosts. 
    """
    ghost_score = 0.0
    food_score = 0.0
    pellet_score = 0.0
    sum_score = 0

    # Distance to each of the ghosts
    for ghosts in currentGameState.getGhostStates():

        ghost_score += ghost_heur(ghosts.getPosition(), \
                                    currentGameState.getPacmanPosition())


    # Distance food - 10x turned out nicely
    food_score += 10 * food_heur(currentGameState.getPacmanPosition(), \
                            currentGameState.getFood())

    # adding pellet score was detrimental
    # pellet_score += pellet_heur(currentGameState.getPacmanPosition(), \
    #                                 currentGameState.getCapsules())


    # Reacting to scared ghosts turned out to be detrimental.
    #     At least, the way I had Pacman reacting to them!
    # newGhostStates = currentGameState.getGhostStates()
    # newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
    # print newScaredTimes[0]
    # if not newScaredTimes[0]:
    #     sum_score = food_score + ghost_score
    # elif newScaredTimes[0] > 3:
    #     sum_score = food_score - ghost_score

    sum_score = food_score + ghost_score

    return sum_score
def pellet_heur(pac_pos, pellets):
    sum_score = 0.0
    p_x, p_y = pac_pos

    for each in pellets:
        pp_x, pp_y = each

        sum_score += (abs(pp_x - p_x) + abs(pp_y - p_y))

    if sum_score == 0:
        return 1

    return 1/sum_score

def food_heur(pac_pos, rem_food):
    sum_score = 0.0
    p_x, p_y = pac_pos

    for each in rem_food.asList():
        f_x, f_y = each
        sum_score += (abs(f_x - p_x) + abs(f_y - p_y))

    if sum_score == 0:
        return 1

    return 1/sum_score

def ghost_heur(ghost_pos, pac_pos):
    sum_score = 0
    g_x, g_y = ghost_pos
    p_x, p_y = pac_pos

    sum_score += abs(g_x - p_x) + abs(g_y - p_y)

    if sum_score < 2:
        # If ghost is very close - Bad state!
        return -float('inf')

    if sum_score > 5:
        return 0

    return -1/sum_score

def check_terminator(game_state, agent):
    count = 0
    for moves in game_state.getLegalActions(agent):
        count = count + 1

    return count

# Abbreviation
better = betterEvaluationFunction
