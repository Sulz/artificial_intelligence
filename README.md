# Artificial Intelligence
This is a collection of assignments from the AI subject.
It is based on the material produced by Berkley:
http://ai.berkeley.edu/home.html

Covering Pacman Projects 1, 2, and 3.

My code for the Pacman contest can be found in my AI_Contest repo.

## How to run
Instructions for each particular project are found in the 'cmd' file.
It lists explicitly the problems being solved and how to run the code yourself.

Note: Windows Bash requires display forwarding (recommend xming + EXPORT DISPLAY=:0.0) 


### 1 - Search (Project 1)
This assignment focused on developing fundamental search algorithms, such as:

 - BFS and DFS

 - Uniform Cost Search

 - A* Search


It also introduced how these can be used to solve pathing problems


### 2 - Multiagent
This assignment introduced approaches using multiple agents, specifically Minimax.
The minimax algorithm is then refined with Alpha-beta pruning and expectimax.

### 3 - Reinforcement
This assignment introduced learning through reinforcement, and gradually built to q-learning.


